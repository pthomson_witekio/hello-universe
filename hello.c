/**
 * @file
 * @author Paul Thomson <pthomson@witekio.com>
 * @brief Simple C application with a Makefile; used for training purposes
 */
 
#include <stdio.h>

/**
 * @brief Prints "Hello Universe", or "Hello " followed by the first command line argument.
 * @param argc Number of command line arguments
 * @param argv Array of command line arguments
 * @return Always 0
 */
int main(int argc, char *argv[])
{
    printf("Hello ");
    
  if (argc > 1)
  {
       printf("%s\n", argv[1]);
  }
  else
  {
      printf("Universe\n");
  }
  
  return 0;
}
