#
# Makefile
#

APP_NAME := hello

${APP_NAME}: hello.c
	@echo [CC] $@
	@${CC} ${CCFLAGS} hello.c -o $@ ${LDFLAGS}

clean:
	@rm -f ${APP_NAME}

install:
	@mkdir -p ${DESTDIR}/${BINDIR}
	@install -m 0755 ${APP_NAME} ${DESTDIR}/${BINDIR}

.PHONY: clean install
